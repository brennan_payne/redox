const _ = require('lodash');
const moment = require('moment')

/*
  pullRequests - Array of pull request
  This separates them based on merged_at
*/
function getMergedPullRequests(pullRequests) {
  // Find merged pull requests, sorted by merged date
  return _.chain(pullRequests)
    .filter(pullRequest => pullRequest.merged_at)
    .sortBy(pullRequest => pullRequest.merged_at)
    .value()
}

function getPullRequestsByWeek(pullRequests) {
  let mergedPullRequests = getMergedPullRequests(pullRequests);

  let firstMergedPR = _.head(mergedPullRequests).merged_at;
  let startOfFirstWeek = moment(firstMergedPR).startOf('week');
  let endOfFirstWeek = moment(firstMergedPR).endOf('week');

  let lastMergedPr = _.last(mergedPullRequests).merged_at;
  let startOfLastWeek = moment(lastMergedPr).startOf('week');
  let endOfLastWeek = moment(lastMergedPr).endOf('week');

  let numberOfWeeks = moment(startOfLastWeek).diff(startOfFirstWeek, 'weeks');
  let weeks = _.times(numberOfWeeks + 1, () => []);

  return _.reduce(mergedPullRequests, (memo, pullRequest) => {
    let week = moment(pullRequest.merged_at).diff(startOfFirstWeek, 'weeks');
    week = parseInt(week, 10);
    memo[week] = _.concat(memo[week], pullRequest);
    return memo;
  }, weeks);

}


module.exports = {
  getMergedPullRequests,
  getPullRequestsByWeek
};
