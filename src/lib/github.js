const axios = require('axios');

const GITHUB_API_URL = 'https://api.github.com';
const GITHUB_ACCESS_TOKEN = process.env.GITHUB_ACCESS_TOKEN
const parseLinkHeader = require('parse-link-header');
const _ = require('lodash');

const githubRequest = axios.create({
  baseURL: GITHUB_API_URL,
  headers: {
    'Authorization': `token ${GITHUB_ACCESS_TOKEN}`
  },
  contentLength: 500000
});

const errorHandler = (response) => {
  let rejection;
  if (response.status === 403 && response.headers['x-ratelimit-remaining'] === 0) {
    rejection = 'Rate Limit Error';
  } else {
    rejection = 'Unhandled Error';
  }
  return Promise.reject(rejection);
}

const successHandler = (response) => {
  // Assume JSON
  return Promise.resolve(response.data);
}

/*
Given an initial link header, an optionally a page from which to start from,
construct the remaining urls to query for this repo.
*/
function constructAllPageUrls(links, startFromPage = 2) {
  let baseUrl = links.next.url;
  let lastPage = parseInt(links.last.page, 10);

  // Captures `page` query param
  // /?page=1 || ?foo=1&page=1
  let pageReg = /([?&])page=[0-9]+/;
  return _.range(startFromPage, lastPage + 1).map((page) => {
    return baseUrl.replace(pageReg, `$1page=${page}`)
  });
}

function fetchRemainingPages(response) {
  let dataPromises = [Promise.resolve(response.data)];
  let links = parseLinkHeader(response.headers['link']);

  // Gather paginated results
  if (_.has(links, 'next.url')) {
    let urls = constructAllPageUrls(links);
    let pagePromises = _.map(urls, (url) => {
      return githubRequest(url).then(successHandler).catch(errorHandler);
    });
    dataPromises = _.concat(dataPromises, pagePromises);
  }

  // Return single array of results
  return Promise.all(dataPromises).then(responses => _.flatten(responses))
}

const github = {
  organizations: {
    retrieveAllRepos: ({org}) => {
      if (!org) {return Promise.reject('Missing an organization')}

      return githubRequest(`/orgs/${org}/repos`)
        .then(successHandler)
        .catch(errorHandler);
    }
  },

  repositories: {
    retrieveAllPullRequests: ({org, repo}) => {
      if (!org) {return Promise.reject('Missing an organization')}
      if (!repo) {return Promise.reject('Missing a repo slug')}

      return githubRequest(`/repos/${org}/${repo}/pulls`, {
        params: {
          state: 'all',
          per_page: 100
        }
      })
      .then(fetchRemainingPages)
      .catch(errorHandler);
    }
  }
}
module.exports = github;
