const _ = require('lodash');
const github = require('./lib/github');
const GITHUB_ORG_SLUG = 'lodash';
const bluebird = require('bluebird');
const fs = require('fs');

function processResults(results) {
  let countByRepo = _.reduce(results, (memo, repos, repoName) => {
    memo[repoName] = _.size(repos);
    return memo;
  }, {});
  console.warn('Count By Repo: ');
  console.warn(countByRepo);
  let total = _.reduce(countByRepo, (runningTotal, repos) => {
    return runningTotal + repos;
  }, 0);
  console.warn('Total Count');
  console.warn(total);
}

function fetchAllRepos() {
  return github.organizations.retrieveAllRepos({
    org: GITHUB_ORG_SLUG
  });
}

/*
  Returns a promise that resolves to:
  {
    repoName: [repos]
  }
*/
function fetchAllPullRequests(repos) {
  /*
    Create object with
    {
      repoName: promiseForRepos
    }
  */
  let pullRequestsByRepoPromise = _.reduce(repos, function(memo, repo) {
    memo[repo.name] = fetchAllPullRequestsForSingleRepo(repo);
    return memo;
  }, {});

  /*
    Use bluebird to resolve promises at values
  */
  return bluebird.props(pullRequestsByRepoPromise);
}

// Small wrapper around grabbing a specific repo from an org
function fetchAllPullRequestsForSingleRepo(repo) {
  return github.repositories.retrieveAllPullRequests({
    org: GITHUB_ORG_SLUG,
    repo: repo.name
  });
}

const start = () => {
  fetchAllRepos()
    .then(fetchAllPullRequests)
    .then(processResults)
    .catch((error) => {
      console.error('UNCAUGHT ERROR IN START()');
      console.error(error);
    })
}

module.exports = start;
