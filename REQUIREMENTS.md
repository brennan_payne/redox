We want to better understand our PR traffic. In particular, we want to see week over week trends of:

  # of PRs merged

  Average and median time from PR creation to merge time
   - For pull requests merged this week, how long were they in review?

  Average and median time from first commit to merge time

  Average and median size of PR
    by # of files
    by # of lines of code
    By # of commits

If we have time, we can optimize for performance or refactor to make the project more production-like.

Ramda is a good org with multiple repos and a manageable set of PRs: https://github.com/ramda
