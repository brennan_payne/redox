### Hello Redox.


#### Prerequisites:

Expects `process.env.GITHUB_ACCESS_TOKEN`, otherwise we will likely hit the 50 request limit on unauthorized requests.

#### Setup:

`yarn install`

`yarn start`

#### Debug:

With chrome DevTools (for node) open, run: `yarn debug`

#### Tests:

For single run: `yarn test`

For TDD (watch mode): `yarn tdd`
