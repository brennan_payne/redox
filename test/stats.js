const expect = require('chai').expect;
const fs = require('fs');
const stats = require('../src/stats');

describe('Stats', function() {
  let data;

  beforeEach(() => {
    let file = fs.readFileSync(`${__dirname}/fixtures/rambda-lens.json`, 'utf8');
    data = JSON.parse(file);
  });

  describe('Sanity Test', function() {
    it('should have 13 pull results', function() {
      expect(data.length).to.equal(13);
    });
  });

  describe('#getMergedPullRequest', function() {
    it('should get the merged pull requests', () => {
      let results = stats.getMergedPullRequests(data);
      expect(results.length).to.equal(10)
    });
  });

  describe('#getPullRequestsByWeek', function() {
    it('should return pull requests by week', () => {
      let results = stats.getPullRequestsByWeek(data);
      expect(results.length).to.equal(71)
    });

    it('should have two pull requests in the first week', () => {
      let results = stats.getPullRequestsByWeek(data);
      expect(results[0].length).to.equal(2)
    });
  });

});
